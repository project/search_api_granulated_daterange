<?php

namespace Drupal\search_api_granulated_daterange\Plugin\search_api\processor\Property;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\search_api\Processor\ConfigurablePropertyBase;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Utility\Utility;

/**
 * Defines an "granulated date range field" property.
 *
 * @see \Drupal\search_api_granulated_daterange\Plugin\search_api\processor\GranulatedDateRangeFields
 */
class GranulatedDateRangeFieldProperty extends ConfigurablePropertyBase {

  use StringTranslationTrait;

  /**
   * Constant for grouping on year.
   */
  const GRANULATED_DATE_YEAR = 6;

  /**
   * Constant for grouping on month.
   */
  const GRANULATED_DATE_MONTH = 5;

  /**
   * Constant for grouping on day.
   */
  const GRANULATED_DATE_DAY = 4;

  /**
   * Constant for grouping on hour.
   */
  const GRANULATED_DATE_HOUR = 3;

  /**
   * Constant for grouping on minute.
   */
  const GRANULATED_DATE_MINUTE = 2;

  /**
   * Constant for grouping on second.
   */
  const GRANULATED_DATE_SECOND = 1;

  /**
   * {@inheritdoc}
   */
  public function getFieldDescription(FieldInterface $field) {
    $configuration = $field->getConfiguration();

    $arguments = [
      '@field' => $configuration['date_range_field'] ?? '',
    ];

    return $this->t('A granulated multi-valued field the daterane field: @field.', $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FieldInterface $field, array $form, FormStateInterface $form_state) {
    $form['date_range_field'] = [
      '#type' => 'radios',
      '#title' => $this->t('Date Range Fields'),
      '#default_value' => $field->getConfiguration()['date_range_field'],
      '#options' => $this->getAvailableDateRangeProperties($field),
      '#required' => TRUE,
    ];

    $form['granularity'] = [
      '#type' => 'radios',
      '#title' => $this->t('Granularity'),
      '#options' => $this->granularityOptions(),
      '#default_value' => $field->getConfiguration()['granularity'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(FieldInterface $field, array &$form, FormStateInterface $form_state) {
    $values = [
      'date_range_field' => $form_state->getValue('date_range_field'),
      'granularity' => $form_state->getValue('granularity'),
    ];
    $field->setConfiguration($values);
  }

  /**
   * Retrieve all properties that the field type is daterange available on the
   * index.
   *
   * The properties will be keyed by combined ID, which is a combination of the
   * datasource ID and the property path. This is used internally in this class
   * to easily identify any property on the index.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The field
   *
   * @return string[]
   *   All the available properties on the index, keyed by combined ID.
   *
   * @throws \Drupal\search_api\SearchApiException
   * @see \Drupal\search_api\Utility::createCombinedId()
   */
  protected function getAvailableDateRangeProperties(FieldInterface $field) {
    $field_options = [];

    $index = $field->getIndex();

    $datasources = $index->getDatasources();

    foreach ($datasources as $datasource_id => $datasource) {
      foreach ($index->getPropertyDefinitions($datasource_id) as $property_path => $property) {
        $combined_id = Utility::createCombinedId($datasource_id, $property_path);

        if (!$property instanceof \Drupal\field\Entity\FieldConfig) {
          continue;
        }

        if ($property->getFieldStorageDefinition()->getType() != 'daterange') {
          continue;
        }

        $label = $datasource->label() . ' » '. $property->getLabel();

        $field_options[$combined_id] = Html::escape($label);
      }
    }

    return $field_options;
  }

  /**
   * Human readable array of granularity options.
   *
   * @return array
   *   An array of granularity options.
   */
  private function granularityOptions() {
    return [
      self::GRANULATED_DATE_YEAR => $this->t('Year'),
      self::GRANULATED_DATE_MONTH => $this->t('Month'),
      self::GRANULATED_DATE_DAY => $this->t('Day'),
      self::GRANULATED_DATE_HOUR => $this->t('Hour'),
      self::GRANULATED_DATE_MINUTE => $this->t('Minute'),
      self::GRANULATED_DATE_SECOND => $this->t('Second'),
    ];
  }

}
