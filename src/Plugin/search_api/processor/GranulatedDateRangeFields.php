<?php

namespace Drupal\search_api_granulated_daterange\Plugin\search_api\processor;

use Drupal\search_api_granulated_daterange\Plugin\search_api\processor\Property\GranulatedDateRangeFieldProperty;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Utility\Utility;

/**
 * Granulated a existing date range field
 *
 * @see \Drupal\search_api_granulated_daterange\Plugin\search_api\processor\Property\GranulatedFieldProperty
 *
 * @SearchApiProcessor(
 *   id = "granulated_date_range_field",
 *   label = @Translation("Granulated date range fields"),
 *   description = @Translation(""),
 *   stages = {
 *     "add_properties" = 20,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class GranulatedDateRangeFields extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Granulated date range field'),
        'description' => $this->t(''),
        'type' => 'datetime_iso8601',
        'is_list' => TRUE,
        'processor_id' => $this->getPluginId(),
      ];
      $properties['granulated_date_range_field'] = new GranulatedDateRangeFieldProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $granulated_date_range_fields = $this->getFieldsHelper()
      ->filterForPropertyPath($item->getFields(), NULL, 'granulated_date_range_field');

    foreach ($granulated_date_range_fields as $granulated_date_range_field) {
      $values = [];

      $configuration = $granulated_date_range_field->getConfiguration();

      $date_range_field = $configuration['date_range_field'];
      $granularity = $configuration['granularity'];

      list(, $property_path) = Utility::splitCombinedId($date_range_field);

      $required_properties = [
        $item->getDatasourceId() => [
          $property_path . ':value' => 'start',
          $property_path . ':end_value' => 'end',
        ],
      ];

      $item_values = $this->getFieldsHelper()
        ->extractItemValues([$item], $required_properties);

      foreach ($item_values as $key => $dates) {
        $start_dates = $dates['start'];
        $end_dates = $dates['end'];

        for ($i = 0, $n = count($start_dates); $i < $n; $i++) {
          $start = $start_dates[$i];
          $end = $end_dates[$i];

          $values[] = $start;

          $daterange_start= new \DateTime($start);
          $daterange_end = new \DateTime($end);
          $daterange_interval = new \DateInterval(self::formatGranularityIntervalSpec($granularity));
          $daterange = new \DatePeriod($daterange_start, $daterange_interval,$daterange_end);
          foreach($daterange as $datepoint){
            $values[]  = $datepoint->format(\DateTime::ISO8601);
          }

          $values[] = $end;
        }
      }

      // Do not use setValues(), since that doesn't preprocess the values
      // according to their data type.
      foreach ($values as $value) {
        $granulated_date_range_field->addValue($value);
      }
    }
  }

  /**
   * Returns a DateItme Interval Spec based on given granularity
   *
   * @param $granularity
   *
   * @return string
   */
  public function formatGranularityIntervalSpec($granularity) {
    switch($granularity) {
      case GranulatedDateRangeFieldProperty::GRANULATED_DATE_YEAR:
        return 'P1Y';
      case GranulatedDateRangeFieldProperty::GRANULATED_DATE_MONTH:
        return 'P1M';
      default:
      case GranulatedDateRangeFieldProperty::GRANULATED_DATE_DAY:
        return 'P1D';
      case GranulatedDateRangeFieldProperty::GRANULATED_DATE_HOUR:
        return 'PT1H';
      case GranulatedDateRangeFieldProperty::GRANULATED_DATE_MINUTE:
        return 'PT1M';
      case GranulatedDateRangeFieldProperty::GRANULATED_DATE_SECOND:
        return 'PT1S';
    }
  }

}
